package br.com.falaai.View

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import br.com.falaai.Model.HomeFragment
import br.com.falaai.constants.Constants
import kotlinx.android.synthetic.main.activity_principal.*
import kotlinx.android.synthetic.main.app_bar_activity_principal.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser


class Activity_principal : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    var myAuth  = FirebaseAuth.getInstance()
    var user: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)
        setSupportActionBar(toolbar)


        user = myAuth!!.currentUser

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        //Carrega conteudo da tela principal ( Boas-Vindas )
        loadMenuHome(fragl = HomeFragment())

    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)

        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_principal, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                startActivity(Intent(this, Activity_principal::class.java))
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.infra_menu -> {
                startActivity(Intent(this@Activity_principal, MapsActivity::class.java))
            }
            R.id.envio_menu -> {
                startActivity(Intent(this@Activity_principal, MeuService_Activity::class.java))
                //startActivity(Intent(this@Activity_principal, Exibir_Activity::class.java))
            }
            R.id.service_menu -> {
                startActivity(Intent(this@Activity_principal, Service_Activity::class.java))
            }

            R.id.sair -> {
                startActivity(Intent(this@Activity_principal, LoginActivity::class.java))
                RemoviAcess(user)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    //Chama tela de boas vinda via Fragment
    private  fun loadMenuHome(fragl:HomeFragment){
        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.fragLayout,fragl)
        fm.commit()
    }

    //Deleta o Token do Usuario e encerra conexão com Firebase
    private fun RemoviAcess(u: FirebaseUser?){

        deletToken(Constants.US_TOKEN_USER)
        if (u!= null){
            myAuth!!.signOut()
            startActivity(Intent(this@Activity_principal, LoginActivity::class.java))
        }else{
            print("Erro ao deslogar usuário!")
        }
    }

    //Deleta o Token do usuário
    private fun deletToken(key:String){
        val pref = this.getSharedPreferences("br.com.falaai.main_act",android.content.Context.MODE_PRIVATE)
        val result = pref.getString(key, "")

        if(result == "" || result.isEmpty()){
            println("Sem token")
        }else{
            pref.edit().remove(key).apply()
            pref.edit().clear().apply()
            println("Toke do usuário deletado")
        }
    }

}
