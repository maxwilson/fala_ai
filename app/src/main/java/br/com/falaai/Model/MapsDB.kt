package br.com.falaai.Model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class MapsDB {

    var lt: String? = null
    var id: String? = null


    constructor() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    constructor(lt: String) {
        this.lt = lt
    }

}