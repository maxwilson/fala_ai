package br.com.falaai.View

import android.app.DialogFragment
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_cadastro.*


class Cadastro_Activity : AppCompatActivity() , View.OnFocusChangeListener {
    var myAuth = FirebaseAuth.getInstance()
    var dia = ""
    lateinit var emailV : EditText
    lateinit var senhaV : EditText
    lateinit var dataNasc : EditText
    lateinit var btn_register : Button

    //Data Picker Exibido no campo data de nascimento ( Focus )
    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        if (hasFocus){
            var newDialog: DialogFragment = br.com.falaai.Model.DatePicker()
            newDialog.show(fragmentManager,"DatePickert")

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastro)

        //Seta um focus no campo data
        edit_dataNasc.setOnFocusChangeListener(this)

        //Pega id de campo do formulario
        emailV = findViewById(R.id.edit_email)
        senhaV = findViewById(R.id.edit_senha)
        //dataNasc = findViewById(R.id.edit_dataNasc)
        btn_register = findViewById(R.id.btn_cadastrar)

        //Button
        btn_register.setOnClickListener{
            val email = emailV.text.toString().trim()
            val senha = senhaV.text.toString().trim()
            Toast.makeText(this,"", Toast.LENGTH_LONG).show()
            if (!validadorRegister(email,senha)){
                signUp(email,senha)
            }else{
                return@setOnClickListener
            }
        }

    }
    //Cria conta do usuários verificando os dados passado no formulario
    private fun signUp(email:String , password:String){
        myAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, OnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this@Cadastro_Activity, "Conta criada com sucesso", Toast.LENGTH_LONG).show()
                        var intent = Intent(this,LoginActivity::class.java)
                        intent.putExtra("id", myAuth.currentUser?.email)
                        startActivity(intent)
                    } else {
                        Toast.makeText(this, "Erro!! " + task.exception?.message, Toast.LENGTH_LONG).show()
                    }
                })
    }
    //Faz uma verificação dos dados passado pelo formulario
    private fun validadorRegister(email: String,password: String): Boolean {

        // Reseta os erros.
        emailV.error = null
        senhaV.error = null

        //Variavel email e senha pegando na VIEW
        val emailStr = emailV.text.toString()
        val passwordStr = senhaV.text.toString()

        var cancel = false
        var focusView: View? = null

        if (passwordStr.isEmpty() && !isPasswordValid(passwordStr)) {
            senhaV.error = getString(R.string.error_invalid_password)
            focusView = senhaV
            cancel = true
            return cancel
        }

        if (TextUtils.isEmpty(emailStr)) {
            emailV.error = getString(R.string.error_field_required)
            focusView = emailV
            cancel = true
            return cancel
        } else if (!isEmailValid(emailStr)) {
            emailV.error = getString(R.string.error_invalid_email)
            focusView = emailV
            cancel = true
            return cancel
        }

        return cancel
    }
    // Verifica se email passado e valido
    private fun isEmailValid(email: String): Boolean {
        return email.contains("@")
    }
    //Verifica se desenha digitada e menor que 4
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 4
    }
}
