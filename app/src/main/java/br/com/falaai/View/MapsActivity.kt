package br.com.falaai.View


import android.app.ProgressDialog
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import br.com.falaai.Model.MapsDB
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private var p: ArrayList<MapsDB> = ArrayList<MapsDB>()
    private var mDatabase: DatabaseReference? = null

    private lateinit var mMap: GoogleMap
    private lateinit var latlng: LatLng
    lateinit var local : EditText
    lateinit var btn_pesquisar : Button
    lateinit var progress: ProgressDialog

    private var strPes = ""

    //Infla um menu dentro da tela serviço
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_principal, menu)
        return true
    }
    //Adciona função ao clica no menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                startActivity(Intent(this, Activity_principal::class.java))
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.maps_infra)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment

        mDatabase = FirebaseDatabase.getInstance().reference
        mapFragment.getMapAsync(this)
        local = findViewById(R.id.edit_local)
        btn_pesquisar = findViewById(R.id.btn_enviar_infra)

        btn_pesquisar.setOnClickListener({
            goToAddress()
        })

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMapClickListener(this@MapsActivity)

        val jampa = LatLng(-7.1588602,-34.8572636)

        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        mMap.addMarker(createMarkers(jampa, "Unipê", "Centro Universitário"))

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(jampa, 15.5f))


    }

    fun createMarkers(latLng: LatLng, title:String, snippet:String): MarkerOptions{
        return MarkerOptions()
                .position(latLng)
                .title(title)
                .snippet(snippet)
        //.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher))
    }

    override fun onMapClick(latlng:LatLng) {
        this.latlng = latlng

        progress = ProgressDialog(this)
        progress.setTitle("Carregando endereço ...")
        progress.show()
        //salvarPos(latlng.toString())
        ReserveLatLngToAddress().execute()
    }

    fun goToAddress(){
        progress = ProgressDialog(this)
        progress.setTitle("Carregando endereço ...")
        progress.show()
        strPes = local.text.toString()
        if (!strPes.isEmpty()){
            ReserveAddressToLatLng().execute()
        }else{
            Toast.makeText(this,"O nome da rua precisa ser preenchido!", Toast.LENGTH_SHORT).show()
        }

    }


    inner class ReserveLatLngToAddress : AsyncTask<Void, Void, Address>(){

        override fun doInBackground(vararg p0: Void?): Address? {
            try {

                var geo = Geocoder(this@MapsActivity)
                var addresses = geo.getFromLocation(this@MapsActivity.latlng.latitude
                        ,this@MapsActivity.latlng.longitude
                        , 1)

                return addresses.get(0)

            }catch (e:Exception){
                return null
            }
        }

        override fun onPostExecute(result: Address?) {

            this@MapsActivity.progress.dismiss()

            if(result != null){
                this@MapsActivity.mMap.addMarker(
                        this@MapsActivity.createMarkers(
                                this@MapsActivity.latlng,
                                result.thoroughfare,
                                result.locality + " - " + result.postalCode
                        )
                )
            }
        }
    }

    inner class ReserveAddressToLatLng : AsyncTask<Void, Void, Address>(){

        override fun doInBackground(vararg p0: Void?): Address? {
            try {

                var geo = Geocoder(this@MapsActivity)
                val addresses = geo.getFromLocationName(this@MapsActivity.strPes, 1)
                print(addresses.get(0))
                return addresses.get(0)

            }catch (e:Exception){
                return null
            }
        }

        override fun onPostExecute(result: Address?) {

            this@MapsActivity.progress.dismiss()

            if(result != null){
                var latlng = LatLng(result.latitude, result.longitude)
                this@MapsActivity.mMap.addMarker(
                        this@MapsActivity.createMarkers(
                                latlng,
                                result.thoroughfare,
                                result.locality + " - " + result.postalCode
                        )
                )
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 18.5f))
            }
        }
    }

    //Sem uso
    fun salvarPos(position:String){
        if (!position.isEmpty()){
            val pb = MapsDB(position)
            mDatabase!!.child("maps").child("0").setValue(pb)
        }
    }

    //Sem uso
    private fun getDataService(){
        //Recupera json do banco do Firebase
        val dataReference = FirebaseDatabase.getInstance().getReference("maps")

        val query = dataReference!!.limitToFirst(1).orderByChild("lt")

        query.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(p0: DataSnapshot) {
                p.clear()

                for(snapshot in p0.children){
                    val Lt = snapshot.child("lt").value.toString()
                    //Toast.makeText(this@MapsActivity,Lt,Toast.LENGTH_SHORT).show()
                    p.add(MapsDB(Lt))
                }

            }
            override fun onCancelled(p0: DatabaseError) {
            }
        })

    }

}