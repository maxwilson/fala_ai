package br.com.falaai.Model

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import br.com.falaai.View.LoginActivity
import com.google.firebase.auth.FirebaseAuth

class Firabase_Auth : AppCompatActivity(){
    var myAuth  = FirebaseAuth.getInstance()

    fun  signOut(){
        //Toast.makeText(this,"Saindo...", Toast.LENGTH_LONG).show()
        myAuth.signOut()
        var intent = Intent(this,LoginActivity::class.java)
       // intent.putExtra("id", myAuth.currentUser?.email)
        startActivity(intent)

    }
}