package br.com.falaai.View

import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import br.com.falaai.constants.Constants

class LoginActivity : AppCompatActivity() {

    var myAuth  = FirebaseAuth.getInstance()
    var br = ConnReceiver()
    var gv : Boolean = false
    lateinit var emailV : EditText
    lateinit var senhaV : EditText
    lateinit var checkBox: CheckBox
    lateinit var btn_register : Button
    lateinit var btn_logar : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        registerReceiver(br, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))

        // Variavel captura do email e senha
        emailV = findViewById(R.id.edit_email)
        senhaV = findViewById(R.id.edit_senha)
        //Checkbox da tela de login
        checkBox = findViewById(R.id.mConectado)
        //Button tela de login
        btn_logar = findViewById(R.id.btn_login)
        btn_register = findViewById(R.id.btn_cadastrar)

    /* ---------------------------------------------------------------------------*/
        //Puxa o token do usuário
        getUs(Constants.US_TOKEN_USER)

        //Verifica se email e senha digita e valido se sim e logado no app
        btn_logar.setOnClickListener {
            view  ->
            val email = emailV.text.toString().trim()
            val senha = senhaV.text.toString().trim()
            if(!validadorLogin(email,senha)){
                signIn(email,senha)
            }else{
                return@setOnClickListener
            }

        }

        //Passa para tela de cadastro
        btn_register.setOnClickListener{
            var intent = Intent(this,Cadastro_Activity::class.java)
            startActivity(intent)
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(br)
    }

    //Efetua o login
    private fun signIn(email:String, password:String){
        Toast.makeText(this,"Autenticando... ", Toast.LENGTH_LONG).show()
        myAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, OnCompleteListener { task ->
                    if(task.isSuccessful){
                        var intent = Intent(this, Activity_principal::class.java)
                        intent.putExtra("id", myAuth.currentUser?.email)
                        startActivity(intent)
                        val userID = myAuth!!.currentUser!!.uid
                        val userToken = myAuth!!.currentUser!!.getIdToken(true)

                        if (checkBox.isChecked){
                            salvarManterConectado(Constants.US_TOKEN_USER, value = userToken.toString())
                        }
                        attUser()

                    }else{
                        Toast.makeText(this,"Erro ao efetuar login "+ task.exception?.message, Toast.LENGTH_LONG).show()
                    }

                })

    }

    /* Validação dos campos digitado pelo usuários */
    private fun validadorLogin(email: String,password: String): Boolean {

        // Reseta os erros.
        emailV.error = null
        senhaV.error = null

        //Variavel email e senha pegando na VIEW
        val emailStr = emailV.text.toString()
        val passwordStr = senhaV.text.toString()

        var cancel = false
        var focusView: View? = null

        if (passwordStr.isEmpty()) {
            senhaV.error = getString(R.string.error_invalid_password)
            focusView = senhaV
            cancel = true
            return cancel
        }else if (!isPasswordValid(passwordStr)){
            return cancel
        }

        if (TextUtils.isEmpty(emailStr)) {
            emailV.error = getString(R.string.error_field_required)
            focusView = emailV
            cancel = true
            return cancel
        } else if (!isEmailValid(emailStr)) {
            emailV.error = getString(R.string.error_invalid_email)
            focusView = emailV
            cancel = true
            return cancel
        }

        return cancel
    }
    private fun isEmailValid(email: String): Boolean {
        return email.contains("@")
    }
    private fun isPasswordValid(password: String): Boolean {
        return password.length < 4
    }

    //Manter conectado salva o token do usuário
    private fun salvarManterConectado(key:String, value:String){
        val pref = this.getSharedPreferences("br.com.falaai.main_act",android.content.Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putString(key,value)
        editor.apply()
    }

    // Limpa a pilha do top
    private fun attUser(){
        val i = Intent(this@LoginActivity,Activity_principal::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(i)
    }

    //Chama o usuário caso tiver token
    private fun getUs(key: String){
        val pref = this.getSharedPreferences("br.com.falaai.main_act",android.content.Context.MODE_PRIVATE)
        val result = pref.getString(key,"")

        if (result.isEmpty() || result == null){
            Toast.makeText(this@LoginActivity,"Necessário realizar login!", Toast.LENGTH_SHORT).show()
        }else{
            intent = Intent(this@LoginActivity, Activity_principal::class.java)
            startActivity(intent)
        }

    }
}
