package br.com.falaai.View

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.ProgressBar
import android.widget.Toast
import br.com.falaai.Adaters.MyAdapter
import br.com.falaai.Model.ServiceModel
import com.google.firebase.database.*
import com.google.firebase.database.FirebaseDatabase.*
import kotlinx.android.synthetic.main.activity_meu_service.*

class MeuService_Activity : AppCompatActivity() {
    private var postos: ArrayList<ServiceModel> = ArrayList<ServiceModel>()
    private lateinit var pb: ProgressBar

    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference

    //Infla um menu dentro da tela MeuEnvio
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_principal, menu)
        return true
    }
    //Adciona função ao clica no menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                startActivity(Intent(this, Activity_principal::class.java))
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meu_service)


        rvEventos.setHasFixedSize(true)
        rvEventos.layoutManager = LinearLayoutManager(this)

        firebaseDatabase = FirebaseDatabase.getInstance()
        myRef = firebaseDatabase.reference

        getDataService()

        val mAdapter: RecyclerView.Adapter<*> = MyAdapter(this@MeuService_Activity, postos){
            Toast.makeText(this@MeuService_Activity, it.toString(), Toast.LENGTH_SHORT).show()
        }
        rvEventos.adapter = mAdapter
}
    private fun getDataService(){
        //Recupera json do banco do Firebase
        val dataReference = getInstance().getReference("servicos")
        //Recupera o ID organizando os cards views pelo id
        val query = dataReference!!.limitToFirst(15).orderByChild("id")

        query.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(p0: DataSnapshot) {
                postos.clear()

                for(snapshot in p0.children){
                    val cep = snapshot.child("cep").value.toString()
                    val rua = snapshot.child("rua").value.toString()
                    val comp = snapshot.child("comp").value.toString()
                    val coment = snapshot.child("coment").value.toString()
                    val tipo = snapshot.child("tipo").value.toString()

                    postos.add(ServiceModel(cep, rua, comp, coment, tipo))
                }
                val myAdapter = MyAdapter(this@MeuService_Activity, postos) {

                }
                rvEventos.adapter = myAdapter
                //pb.visibility
            }
            override fun onCancelled(p0: DatabaseError) {
            }
        })

    }
}



