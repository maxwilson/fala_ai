package br.com.falaai.View

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import br.com.falaai.Model.ServiceModel
import br.com.falaai.View.R.id.action_settings
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.act_service.*

class Service_Activity : AppCompatActivity() {

    private var mDatabase: DatabaseReference? = null
    lateinit var idEdtV :EditText
    lateinit var cepV: TextInputEditText
    lateinit var ruaV: TextInputEditText
    lateinit var compV: TextInputEditText
    lateinit var comentV: TextInputEditText
    lateinit var itemTipoV : Spinner
    lateinit var btn_enviar : Button

    //Infla um menu dentro da tela serviço
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_principal, menu)
        return true
    }
    //Adciona função ao clica no menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> {
                startActivity(Intent(this, Activity_principal::class.java))
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_service)

        mDatabase = FirebaseDatabase.getInstance().reference

        /* Capturando os ids do formulario */
        idEdtV = findViewById(R.id.edit_id)
        cepV = findViewById(R.id.edit_cep)
        ruaV = findViewById(R.id.edit_rua)
        compV = findViewById(R.id.edit_complemento)
        comentV = findViewById(R.id.edit_comentario)
        itemTipoV = findViewById(R.id.list_service)

        btn_enviar = findViewById(R.id.btn_enviar_s)

        btn_enviar.setOnClickListener({

            val idEdt = idEdtV.text.toString().trim()
            val cep = cepV.text.toString().trim()
            val rua = ruaV.text.toString().trim()
            val comp = compV.text.toString().trim()
            val coment = comentV.text.toString()

            if (!idEdt.isEmpty()){
                inserServico(idEdt,cep,rua,comp,coment)
            }else{
                Toast.makeText(this,"ERRO: Necessário um ID", Toast.LENGTH_SHORT).show()
            }
        })

    val adapter: ArrayAdapter<CharSequence> =
            ArrayAdapter.createFromResource(this,R.array.arraylist_service,android.R.layout.simple_spinner_dropdown_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        list_service.adapter = adapter
        list_service.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(this@Service_Activity, R.string.Select,Toast.LENGTH_LONG).show()
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //Toast.makeText(this@Service_Activity, position.toString() ,Toast.LENGTH_LONG).show()
                // Quando o item estiver selecionado realizar ?
            }
        }
    }

    //Insere no banco do FireBase
    fun inserServico(idServ : String,cep:String,rua:String, comp:String, coment:String ){
        //Captura qual seleção fez no spinner
        var i = list_service.selectedItem
        var tipoB:String = i.toString()

        //Verifica o valor se valor seleciona e o padrão
        if (tipoB == "Selecione") {
             tipoB = "Denuncia"
        }else{
             tipoB = i.toString()
        }
        //Recebe uma variavel do tipo Objet ( ServiceModel ) e insere no banco
        val serv = ServiceModel(cep,rua,comp,coment,tipoB)
        mDatabase!!.child("servicos").child(idServ).setValue(serv)
        Toast.makeText(this,"Dados salvo com sucesso!", Toast.LENGTH_SHORT).show()
        startActivity(Intent(this@Service_Activity, Service_Activity::class.java))
    }

}
