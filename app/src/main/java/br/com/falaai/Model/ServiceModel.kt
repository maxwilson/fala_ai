package br.com.falaai.Model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class ServiceModel {
    var id : Int = 0
    var cep :String? = null
    var rua :String? = null
    var comp :String? = null
    var coment :String? = null
    var tipo :String? = null

    constructor(){
    }
    constructor(cep:String?, rua:String?, comp:String?, coment:String?, tipo:String?){
        this.cep = cep
        this.rua = rua
        this.comp = comp
        this.coment = coment
        this.tipo = tipo
    }



}