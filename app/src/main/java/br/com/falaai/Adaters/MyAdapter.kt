package br.com.falaai.Adaters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.falaai.Model.ServiceModel
import br.com.falaai.View.R
import kotlinx.android.synthetic.main.activity_exibir_.view.*
import java.util.ArrayList

class MyAdapter constructor(val context: Context, private val eventos: ArrayList<ServiceModel>?,
                            val clickListener:(ServiceModel)->Unit) :
        RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.activity_exibir_, parent, false)
        val vh = ViewHolder(v)
        return vh
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        eventos?.let{
            var evento = eventos[position]
            holder.itemView.tv_cep.text = evento.cep
            holder.itemView.tv_rua.text = evento.rua
            holder.itemView.tv_coment.text = evento.coment
            holder.itemView.tv_comp.text = evento.comp
            holder.itemView.tv_tipo.text = evento.tipo
            holder.itemView.setOnClickListener { clickListener(eventos[position]) }
        }
    }

    override fun getItemCount(): Int {
        if (eventos != null) {
            return eventos.size
        }else
            return 0
    }
}