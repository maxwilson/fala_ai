package br.com.falaai.View

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.widget.Toast

class ConnReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        var cm:ConnectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE)
                    as ConnectivityManager
        var netInfo = cm.activeNetworkInfo

        if (netInfo != null && netInfo.isConnectedOrConnecting){
            //Log.i("BRTESTE", "Conectado com internet")
            Toast.makeText(context,"Conectado com internet", Toast.LENGTH_LONG).show()
        }else{
            //Log.i("BRTESTE","Sem conexão com internet")
            Toast.makeText(context,"Desconectado da internet", Toast.LENGTH_LONG).show()
        }
    }
}